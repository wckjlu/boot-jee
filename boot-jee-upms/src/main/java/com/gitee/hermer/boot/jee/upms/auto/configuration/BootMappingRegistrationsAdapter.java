package com.gitee.hermer.boot.jee.upms.auto.configuration;

import org.springframework.boot.autoconfigure.web.WebMvcRegistrationsAdapter;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.gitee.hermer.boot.jee.upms.mapping.BootMappingHandler;


@Configuration
public class BootMappingRegistrationsAdapter extends WebMvcRegistrationsAdapter{
	
	@Override
	public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
		return new BootMappingHandler(); 
	}

}
