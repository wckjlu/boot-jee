package com.gitee.hermer.boot.jee.orm.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "com.boot.jee.orm.mybatis")  
public class OrmIBatisProperties {
	
	private String configLocations;
	private String mapperLocations;
	
	@ConfigurationProperties(value="config-locations")
	public String getConfigLocations() {
		return configLocations;
	}
	public void setConfigLocations(String configLocations) {
		this.configLocations = configLocations;
	}
	@ConfigurationProperties(value="mapper-locations")
	public String getMapperLocations() {
		return mapperLocations;
	}
	public void setMapperLocations(String mapperLocations) {
		this.mapperLocations = mapperLocations;
	}
	
	
	
	
	
}
