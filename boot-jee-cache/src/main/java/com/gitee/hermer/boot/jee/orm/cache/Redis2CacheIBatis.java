package com.gitee.hermer.boot.jee.orm.cache;

import java.text.MessageFormat;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.ibatis.cache.Cache;

import com.gitee.hermer.boot.jee.cache.redis.RedisCacheService;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;

public class Redis2CacheIBatis extends UtilsContext implements Cache{
	
	/** The ReadWriteLock. */
	private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

	private String id; // 目录标识


	public Redis2CacheIBatis(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return this.id;
	}

	/**
	 * 
     * @Description: 从目录中根据key取查询缓存
	 * @param  key
	 * @return 缓存数据
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:12:49
	 */
	@Override
	public Object getObject(Object key) {
		String cacheKey = String.valueOf(key.hashCode());
		Object value = RedisCacheService.get(id, cacheKey);
		debug(MessageFormat.format("{0} >> Cache {1}:{2}", id,
				value == null ? "Miss" : "Hit", value));

		return value;
	}

	@Override
	public ReadWriteLock getReadWriteLock() {
		return this.readWriteLock;
	}

	/**
	 * 
	 * @Description: 设置数据至缓存中
	 * @param key
	 * @param value   
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:13:47
	 */
	@Override
	public void putObject(Object key, Object value) {
		String cacheKey = String.valueOf(key.hashCode());
		RedisCacheService.save(id, cacheKey, value);
	}

	/**
	 * 
	 * @Description: 从缓存中删除指定 key 数据
	 * @param key
	 * @return   
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:29:58
	 */
	@Override
	public Object removeObject(Object key) {
		String cacheKey = String.valueOf(key.hashCode());
		RedisCacheService.remove(id, cacheKey);
		return null;
	}

	/**
	 *  
	 * @Description: 清空当前 Cache 实例中的该目录下所有缓存数据
	 * @param    
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:14:14
	 */
	@Override
	public void clear() {
		RedisCacheService.clear(id);
	}
	/**
	 * 
	 * @Description: TODO 获取缓存大小
	 * @param @return   
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:14:31
	 */
	@Override
	public int getSize() {
		return RedisCacheService.size(id);
	}


}
