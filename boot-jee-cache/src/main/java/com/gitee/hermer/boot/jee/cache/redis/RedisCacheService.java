package com.gitee.hermer.boot.jee.cache.redis;


import java.util.List;

import com.gitee.hermer.boot.jee.cache.BootCache;
import com.gitee.hermer.boot.jee.cache.CacheChannel;
import com.gitee.hermer.boot.jee.commons.log.Logger;
import com.gitee.hermer.boot.jee.commons.verify.Assert;
/**
 * 
 * @ClassName: CacheService
 * @Description: Redis缓存实现
 * @author:  涂孟超
 * @date: 2017年10月6日 下午5:15:28
 */

public class RedisCacheService{

	/** The cache service reference. */
	protected static final CacheChannel CACHE_SERVICE = BootCache.getChannel();

	public static final String CACHE_LOGIN_INFO = "LOGIN_INFO";
	public static final String CACHE_CLIENT_INFO = "CLIENT_INFO";

	private static Logger log =  Logger.getLogger(RedisCacheService.class);

	/**
	 * 
	 * @Description: 将对象缓存
	 * @param id 目录标识
	 * @param key
	 * @param val 
	 * @return void  
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:36:32
	 */
	public static  void save(String id,Object key,Object val)
	{
		Assert.hasText(id);
		Assert.notNull(key);
		CACHE_SERVICE.set(id,key,val);
	}
	
	/**
	 * 
	 * @Description: 缓存中取对象
	 * @param id 目录标识
	 * @param key 
	 * @return Object  
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:05:02
	 */
	public static <T> T get(String id,Object key)
	{
		Assert.hasText(id);
		Assert.notNull(key);
		return (T) CACHE_SERVICE.get(id,key).getValue();
	}
	/**
	 * 
	 * @Description: 缓存删除对象
	 * @param id 目录标识
	 * @param key    
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:05:40
	 */
	public static void remove(String id,Object key)
	{
		Assert.hasText(id);
		Assert.notNull(key);
		CACHE_SERVICE.evict(id,key);
	}
	
	public static Integer ping(){
		RedisCacheService.save("test", 1, 1);
		return RedisCacheService.get("test", 1);
	}
	
	/**
	 * 
	 * @Description: 获取该目录下所有key
	 * @param id 目录标识
	 * @return List 
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:38:20
	 */
	public static List keys(String id)
	{
		Assert.hasText(id);
		return CACHE_SERVICE.keys(id);
	}
	/**
	 * 
	 * @Description: 获取该目录的大小
	 * @param id 目录标识
	 * @return  
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:39:01
	 */
	public static int size(String id)
	{
		Assert.hasText(id);
		if(id != null && CACHE_SERVICE.keys(id)!=null)
			return CACHE_SERVICE.keys(id).size();
		else
			return 0;
	}
	/**
	 * 
	 * @Description: 移除该目录所有数据
	 * @param id  目录标识
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:40:16
	 */
	public static void clear(String id)
	{
		Assert.hasText(id);
		if(size(id) > 0)
			CACHE_SERVICE.clear(id);
	}

	//	public static Long lock(String key, String id, long expire) {
	//		Jedis jedis = RedisCacheProvider.getResource();
	//		try{
	//			StringCache cache = new StringCache("if (redis.call('exists', KEYS[1]) == 0) then redis.call('hset', KEYS[1],ARGV[1], 1); ")
	//					.append("redis.call('pexpire', KEYS[1], ARGV[2]); return nil; end; ")
	//					.append("if (redis.call('hexists', KEYS[1], ARGV[1]) == 1) then redis.call('hincrby', KEYS[1], ARGV[1], 1); ")
	//					.append("redis.call('pexpire', KEYS[1], ARGV[2]); return nil; end; return redis.call('pttl', KEYS[1]);");
	//			Long result = (Long) jedis.eval(cache.toString(), CollectionUtils.newArrayList(key), CollectionUtils.newArrayList(id));
	//			jedis.expire(key, (int)expire);
	//			return result;
	//		}catch (Exception e) {
	//			log.error(e.getMessage(),e);
	//		}finally {
	//			RedisCacheProvider.returnResource(jedis, false);
	//		}
	//		return 0l;
	//	}
	//
	//	public static Long unlock(String key, String id) {
	//		Jedis jedis = RedisCacheProvider.getResource();
	//		try{
	//			StringCache cache = new StringCache("if (redis.call('exists', KEYS[1]) == 0) then return 0; end; ")
	//					.append("if (redis.call('hexists', KEYS[1], ARGV[1]) == 0) then return 0; end; ")
	//					.append("local counter = redis.call('hincrby', KEYS[1], ARGV[1], -1); ")
	//					.append("if (counter > 0) then return 1; ").append( "else ")
	//					.append("redis.call('del', KEYS[1]); return 1; end; return -1;");
	//			return (Long) jedis.eval(cache.toString(), CollectionUtils.newArrayList(key), CollectionUtils.newArrayList(id)); 
	//		}catch (Exception e) {
	//			log.error(e.getMessage(),e);
	//		}finally {
	//			RedisCacheProvider.returnResource(jedis, false);
	//		}
	//		return 0l;
	//	}



}
