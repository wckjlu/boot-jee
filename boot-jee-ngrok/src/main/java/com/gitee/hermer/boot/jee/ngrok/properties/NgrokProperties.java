package com.gitee.hermer.boot.jee.ngrok.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "com.boot.jee.ngrok") 
public class NgrokProperties {

    /**
     * 服务器地址
     */
    private String serverAddress = "";
    /**
     * 服务器端口
     */
    private int serverPort = 4443;
    /**
     * 协议
     */
    private String proto = "http";
    /**
     * 自定义子域名
     */
    private String subdomain;
    /**
     * 自定义域名
     */
    private String hostname;
    private int remotePort;
    private String httpAuth;
    
    @ConfigurationProperties(value="serevr-address")
    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }
    @ConfigurationProperties(value="server-port")
    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public String getProto() {
        return proto;
    }

    public void setProto(String proto) {
        this.proto = proto;
    }

    public String getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(String subdomain) {
        this.subdomain = subdomain;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
    @ConfigurationProperties(value="remote-port")
    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }
    @ConfigurationProperties(value="http-auth")
    public String getHttpAuth() {
        return httpAuth;
    }

    public void setHttpAuth(String httpAuth) {
        this.httpAuth = httpAuth;
    }
}
