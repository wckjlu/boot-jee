package com.gitee.hermer.boot.jee.cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.gitee.hermer.boot.jee.cache.exception.CacheException;

public class CacheManager {
	
	@Autowired
	@Qualifier("l1_provider")
	private CacheProvider l1_provider;
	
	@Autowired
	@Qualifier("l2_provider")
	private CacheProvider l2_provider;
	
	@Autowired
	private CacheListener listener;
	
	public CacheManager(){
	}
	
	
	public final  Object get(int level, String name, Object key) throws CacheException{
		if(name!=null && key != null) {
            Cache cache = getCache(level, name, false);
            if (cache != null)
                return cache.get(key);
        }
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public final  <T> T get(int level, Class<T> resultClass, String name, Object key) throws CacheException{
		
		if(name!=null && key != null) {
            Cache cache =getCache(level, name, false);
            if (cache != null)
                return (T)cache.get(key);
        }
		return null;
	}
	
	
	public final  void set(int level, String name, Object key, Object value) throws CacheException{
		
		if(name!=null && key != null && value!=null) {
            Cache cache =getCache(level, name, true);
            if (cache != null)
                cache.put(key,value);
        }
	}
	
	public final  void set(int level, String name, Object key, Object value, Integer expireInSec) throws CacheException{
		
		if(name!=null && key != null && value!=null) {
            Cache cache =getCache(level, name, true);
            if (cache != null)
                cache.put(key, value, expireInSec);
        }
	}
	
	
	public final  void evict(int level, String name, Object key) throws CacheException{
		
		if(name!=null && key != null) {
            Cache cache =getCache(level, name, false);
            if (cache != null)
                cache.evict(key);
        }
	}
	
	
	@SuppressWarnings("rawtypes")
	public final  void batchEvict(int level, String name, List keys) throws CacheException {
		if(name!=null && keys != null && keys.size() > 0) {
            Cache cache =getCache(level, name, false);
            if (cache != null)
                cache.evict(keys);
        }
	}

	
	public final  void clear(int level, String name) throws CacheException {
        Cache cache =getCache(level, name, false);
        if(cache != null)
        	cache.clear();
	}
	
	
	@SuppressWarnings("rawtypes")
	public final  List keys(int level, String name) throws CacheException {
        Cache cache =getCache(level, name, false);
		return (cache!=null)?cache.keys():null;
	}
	
	
	
	
	private final  Cache getCache(int level, String cache_name, boolean autoCreate) throws CacheException {
		return ((level==1)?l1_provider:l2_provider).buildCache(cache_name, autoCreate, listener);
	}
	
	public final void shutdown(int level){
		((level==1)?l1_provider:l2_provider).stop();
	}

}
